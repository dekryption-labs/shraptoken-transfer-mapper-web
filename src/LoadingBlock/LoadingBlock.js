import { memo } from 'react';
import styles from './LoadingBlock.module.css';
import CircularProgress from '@mui/material/CircularProgress';
import Modal from '../Modal/Modal';

const LoadingBlock = ({ opened }) => {
    return (
        <Modal opened={opened} close={null} noWrap={true} additionalClasses={styles.container}>
            <div className={styles.title}>Loading</div>
            <CircularProgress style={{ color: 'white' }} size='8em' />
        </Modal>
    );
};

export default memo(LoadingBlock);
