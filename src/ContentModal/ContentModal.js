import { memo } from 'react';
import clsx from 'clsx';
import styles from './ContentModal.module.css';
import CloseIcon from '@mui/icons-material/Close';
import Modal from '../Modal/Modal';

const ContentModal = ({ opened, close, title, children }) => {
    return (
        <Modal opened={opened} close={close} additionalClasses={styles.wrap}>
            <header className={styles.header}>
                <span className={clsx(styles.title)} style={{ textAlign: 'center' }}>{title}</span>
            </header>
            {children}
        </Modal>
    );
};

export default memo(ContentModal);
