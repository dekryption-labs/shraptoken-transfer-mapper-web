import { DECIMALS } from "./constants";
import numberWithCommas from "./prettyNumber";

export default async function convertTokenTransfersToChart(transfers, walletNames, amountFilter) {
    let holders = {};
    let netTransfers = [];
    console.log('transfers');
    // console.log(transfers);
    const transactionHashes = Object.keys(transfers);
    console.log(transactionHashes);
    console.log('Creating graph...');
    for (let i = 0; i < transactionHashes.length; i++) {
        const txid = transactionHashes[i];
        // const thisTransfer = transfers[transactionHashes[i]];
        const realValue = parseFloat(transfers[transactionHashes[i]].value) / Math.pow(10, DECIMALS);
        if (realValue < amountFilter) {
            continue;
        }
        const from = transfers[transactionHashes[i]].from_address;
        const to = transfers[transactionHashes[i]].to_address;
        const fromLowercase = from.toLowerCase();
        const toLowercase = to.toLowerCase();
        // fill nodes
        if (!holders[from]) holders[from] = { id: from, label: walletNames[fromLowercase] ? walletNames[fromLowercase] : from };
        if (!holders[to]) holders[to] = { id: to, label: walletNames[toLowercase] ? walletNames[toLowercase] : to };
        // fill edges
        netTransfers.push({ id: txid, source: from, target: to, label: numberWithCommas(realValue.toString()) });
    }
    console.log('Done');

    return { holders: Object.values(holders), netTransfers: netTransfers };
}
