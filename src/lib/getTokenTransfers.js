import axios from "axios";

export default async function getTokenTransfers(startTimestamp, endTimestamp) {
    let transfers = {};
    let holders = {};
    try {
        const r = await axios.get(`https://us-central1-token-history.cloudfunctions.net/getTransferData?start=${startTimestamp}&end=${endTimestamp}`);
        transfers = r.data['transfers'];
        holders = r.data['holders'];
    } catch (err) {
        console.error(err);
        throw 'T';
    }
    return { transfers: transfers, holders: Object.keys(holders) };
};
