import { ethers } from "ethers";
import ERC20_ABI from "../abi/IERC20.abi.json";
import getBlockByTimestamp from "./getBlockByTimestamp";

export default async function getAllTokenBalances(wallets, timestamp, decimals, callback) {
    let newNodes = [];
    const block = await getBlockByTimestamp(timestamp);
    const provider = ethers.getDefaultProvider('https://rpc.ankr.com/avalanche');
    // array to store all balance requests
    let proms = [];
    // let results = {};
    const erc20 = new ethers.Contract(process.env.REACT_APP_TOKEN_ADDRESS, ERC20_ABI, provider);
    for (const w of wallets) {
      // save request in array of Promises
      proms.push(
        erc20.balanceOf(w.id, {
          blockTag: +block,
        })
      );
    }
    // actually requests all balances simultaneously
    const promiseResults = await Promise.allSettled(proms);
    // loop through all responses to format response
    for (let index = 0; index < promiseResults.length; index++) {
      // transforms balance to decimal
      const bal = parseFloat(promiseResults[index].value) / Math.pow(10, decimals);
      // save balance with token name and symbol
      if (bal > 0) newNodes.push({ id: wallets[index].id, label: bal });
    }
    // console.log('results');
    // console.log(newNodes);
    callback(newNodes);
};
