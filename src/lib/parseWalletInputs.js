export default function parseWalletInputs(currentNodes, walletNames) {
    let newNodes = [];

    for (let i = 0; i < currentNodes.length; i++) {
        const newName = walletNames[currentNodes[i].id.toLowerCase()];
        newNodes.push({ id: currentNodes[i].id, label: newName ? newName : currentNodes[i].id });
    }

    return newNodes;
};
