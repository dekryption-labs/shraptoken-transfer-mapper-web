import axios from "axios"

export default async function getBlockByTimestamp(timestamp) {
    // API endpoint from docs
    const endpoint = `https://api.routescan.io/v2/network/mainnet/evm/43114/etherscan/api?module=block&action=getblocknobytime&timestamp=${timestamp}&closest=before`;
    // send requests using axios
    const res = await axios.get(endpoint);
    // reponse received in data.result
    return parseInt(res.data.result);
};
