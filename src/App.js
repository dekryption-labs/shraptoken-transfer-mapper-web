import "./styles.css";
import { GraphCanvas, darkTheme } from "reagraph";
import { useEffect, useState } from "react";
import getTokenTransfers from "./lib/getTokenTransfers";
// import getAllTokenBalances from "./lib/getAllTokenBalances";
import convertTokenTransfersToChart from "./lib/convertTokenTransfersToChart";
import axios from "axios";
// import { DECIMALS } from "./lib/constants";
import LoadingBlock from "./LoadingBlock/LoadingBlock";
import MessageModal from "./MessageModal/MessageModal";
import DateInput from "./DateInput/DateInput";
// import { DAY_SECS } from "./lib/constants";
import ReactFileReader from 'react-file-reader';
import parseWalletInputs from "./lib/parseWalletInputs";
import TextInput from "./TextInput/TextInput";

export default function App() {
  const TIME_SPAN_LIMIT = 604800; // 1 week span limit on transfers

  const [toTimestamp, setToTimestamp] = useState(0);
  const [fromTimestamp, setFromTimestamp] = useState(0);
  const [fromLimit, setFromLimit] = useState(0);
  const [toLimit, setToLimit] = useState(0);
  const [holders, setHolders] = useState([]);
  const [transfers, setTransfers] = useState([]);
  const [openLoadingBlock, setOpenLoadingBlock] = useState(false);
  const [openMessageModal, setOpenMessageModal] = useState(false);
  const [modalMessage, setModalMessage] = useState('An error has occurred.');
  const [walletNames, setWalletNames] = useState({});
  const [amountFilter, setAmountFilter] = useState(0);

  const initSetup = async () => {
    setOpenLoadingBlock(true);
    const initToTimestamp = await axios.get('https://us-central1-token-history.cloudfunctions.net/getLastTimestamp');
    const initFromTimestamp = await axios.get('https://us-central1-token-history.cloudfunctions.net/getFirstTimestamp');
    setFromTimestamp(parseInt(initFromTimestamp.data['timestamp']));
    setToTimestamp(parseInt(initFromTimestamp.data['timestamp']) + TIME_SPAN_LIMIT);
    setFromLimit(parseInt(initFromTimestamp.data['timestamp']));
    setToLimit(parseInt(initToTimestamp.data['timestamp']));
    await createGraph(parseInt(initFromTimestamp.data['timestamp']), parseInt(initFromTimestamp.data['timestamp']) + TIME_SPAN_LIMIT);
  };

  const createGraph = async (ft, tt) => {
    try {
      setOpenLoadingBlock(true);
      const newData = await getTokenTransfers(ft, tt);
      if (!newData.transfers) {
        setHolders([]);
        setTransfers([]);
        setOpenLoadingBlock(false);
        console.log('no transfers!');
        console.log('done');
        return;
      }
      const graphData = await convertTokenTransfersToChart(newData.transfers, walletNames, amountFilter);
      setHolders(graphData.holders);
      setTransfers(graphData.netTransfers);
    } catch (e) {
      if (e.message === 'T') {
        setModalMessage('Token transfers could not be populated. Try decreasing the date range.');
      } else {
        setModalMessage('An error has occurred. Please refresh the page and try again.');
      }
    }
    console.log('done');
    // getAllTokenBalances(graphData.holders, tt, DECIMALS, setHolders);
    setOpenLoadingBlock(false);
  };

  const onChangeDateFrom = (e) => {
    const d = new Date(e.target.value);
    setFromTimestamp(Math.floor(d.getTime() / 1000.0));
    console.log(Math.floor(d.getTime() / 1000.0));
  };

  const onChangeDateTo = (e) => {
    const d = new Date(e.target.value);
    setToTimestamp(Math.floor(d.getTime() / 1000.0));
    console.log(Math.floor(d.getTime() / 1000.0));
  };

  const onChangeAmountFilter = (e) => {
    const a = parseFloat(e.target.value);
    if (isNaN(a)) {
      setAmountFilter(0);
    } else {
      setAmountFilter(a);
    }
  };

  const onSubmit = async () => {
    if (fromTimestamp >= toTimestamp) {
      setModalMessage('From cannot be before to');
      setOpenMessageModal(true);
      return;
    } else if (fromTimestamp < fromLimit) {
      setModalMessage('From must be after ' + (new Date(fromTimestamp * 1000)).toISOString());
      setOpenMessageModal(true);
      return;
    } else if (toTimestamp > toLimit) {
      setModalMessage('From cannot be before to ' + + (new Date(toTimestamp * 1000)).toISOString());
      setOpenMessageModal(true);
      return;
    }
    await createGraph(fromTimestamp, toTimestamp);
  };

  const onUploadJson = (e) => {
    console.log(e[0]);
    setOpenLoadingBlock(true);
    try {
      const ff = new FileReader();
      ff.readAsText(e[0]);
      ff.onload = function() {
        const result = ff.result;
        console.log(result);
        const o = JSON.parse(result);
        let convertedToLower = {};
        const cc = Object.keys(o);
        for (let i = 0; i < cc.length; i++) {
          convertedToLower[cc[i].toLowerCase()] = o[cc[i]];
        }
        setWalletNames(convertedToLower);
        const w = parseWalletInputs(holders, convertedToLower);
        setHolders(w);
        setOpenLoadingBlock(false);
      };
    } catch (e) {
      setModalMessage('JSON could not be parsed. Please try a different file.');
      setOpenMessageModal(true);
      setOpenLoadingBlock(false);
    }
  };

  const onNodeClick = async (e) => {
    // console.log(e);
    await navigator.clipboard.writeText(e.id);
  };

  const resetDates = () => {
    setFromTimestamp(fromLimit);
    setToTimestamp(fromLimit + TIME_SPAN_LIMIT);
  };

  useEffect(() => {
    initSetup();
  }, []);

  // const exampleNodes = [
  //   {
  //     id: "n-1",
  //     label: "1",
  //   },
  //   {
  //     id: "n-2",
  //     label: "2",
  //   },
  //   {
  //     id: "n-3",
  //     label: "3",
  //   },
  //   {
  //     id: "n-4",
  //     label: "4",
  //   },
  // ];
  // const exampleEdges = [
  //   {
  //     id: "1->2",
  //     source: "n-1",
  //     target: "n-2",
  //     label: "Edge 1-2",
  //   },
  //   {
  //     id: "1->3",
  //     source: "n-1",
  //     target: "n-3",
  //     label: "Edge 1-3",
  //   },
  //   {
  //     id: "1->4",
  //     source: "n-1",
  //     target: "n-4",
  //     label: "Edge 1-4",
  //   },
  // ];

  return (
    <div className="App">
      <GraphCanvas
        theme={darkTheme}
        nodes={holders}
        edges={transfers}
        labelType="all"
        draggable={true}
        onNodeClick={onNodeClick}
      />
      <div className="footer">
        <DateInput title={'Start Date'} startLimit={fromLimit} endLimit={toLimit} dateValue={fromTimestamp} onChangeDate={onChangeDateFrom} reset={resetDates} />
        <DateInput title={'End Date'} startLimit={fromLimit} endLimit={toLimit} dateValue={toTimestamp} onChangeDate={onChangeDateTo} reset={resetDates} />
        <TextInput title="Amount Filter (greater than)" value={amountFilter.toString()} onChange={onChangeAmountFilter} />
        <button onClick={onSubmit}>Generate</button>
        <ReactFileReader fileTypes={[".json"]} handleFiles={onUploadJson}>
          <button>Upload Wallets</button>
        </ReactFileReader>
      </div>
      <LoadingBlock opened={openLoadingBlock} />
      <MessageModal opened={openMessageModal} close={() => { setOpenMessageModal(false); }} title={'ERROR'} description={modalMessage} />
    </div>
  );
}
