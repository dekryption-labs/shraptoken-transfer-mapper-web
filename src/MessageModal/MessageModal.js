import { memo } from 'react';
import ContentModal from '../ContentModal/ContentModal';
import classes from './MessageModal.module.css';

const MessageModal = ({ opened, close, title, description }) => {
    return (
        <ContentModal opened={opened} close={close} title={title}>
            <div className={classes.wrap}>
                <div className={classes.body}>
                    {description}
                </div>
                <button
                    onClick={close}
                    className={classes.btn}
                >
                    <span>OK</span>
                </button>
            </div>
        </ContentModal>
    );
};

export default memo(MessageModal);
