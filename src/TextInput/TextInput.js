const TextInput = ({ title, value, onChange }) => {
    return (
        <div>
            <div>{title}</div>
            <input type="text" value={value.toString()} onChange={onChange} />
        </div>
    )
};

export default TextInput;
