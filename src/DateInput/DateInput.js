const DateInput = ({ title, dateValue, onChangeDate, startLimit, endLimit, reset }) => {
    return (
        <div>
            <div>{title}</div>
            <input type="datetime-local" value={new Date(dateValue * 1000).toISOString().slice(0,16)} onChange={onChangeDate} min={startLimit} max={endLimit} onError={reset} onErrorCapture={reset} />
        </div>
    )
};

export default DateInput;
